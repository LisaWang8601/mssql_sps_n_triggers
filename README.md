Welcome to My First Repo
-------------------------------
This repo is an assignment for learning Bitbucket.

For running the SQL query in this repo, Microsoft SQL Server Management Studio(SSMS) 14 or above is needed. First open the query in SSMS, select lines from 1 to 182 and execute. That will create a database called CarInsurance, create 8 tables and populate them each with about 10 rows of sample data. Then the 2 stored procedures and 1 trigger can be executed. The comments in the query explained each block's functions.