﻿-- Bussiness Rules: (There are more rules but here I only present what I utilized in the procedures)
--1. when inserting a new policy ( or a new customer), check if it is already in the database
--2. A trigger set up whenever a new claim is created (an empolyee inserting a new row in the table Claims) , the trigger calculates the new Monthly Premium and alter the table.
--3. Report can be printed from any date range using a stored procedure;

PRINT '******************************';
PRINT 'WangLisa_ASN7.sql Starting';
PRINT '******************************'

GO

USE Master;
SET NOCOUNT ON;

GO

PRINT '>>> Does a CarInsurance database already exist?';

GO
IF EXISTS (SELECT "name"
           FROM Sysdatabases
           WHERE "name" = 'CarInsurance')    
    BEGIN
        PRINT '>>> Yes, a CarInsurance database already exists';
        PRINT '>>> Rolling back pending CarInsurance transactions';
         
        ALTER DATABASE  CarInsurance
            SET SINGLE_USER
            WITH ROLLBACK IMMEDIATE;
 
        PRINT '>>> Dropping the existing CarInsurance database';   
        
        DROP DATABASE CarInsurance;
    END
ELSE
    BEGIN
        PRINT '>>> No, there is no CarInsurance database';    
    END
GO

PRINT '>>> Creating a new CarInsurance database';
GO

CREATE DATABASE CarInsurance; 
GO

USE CarInsurance;

-- Create table Customers which has no Foreign Key
CREATE TABLE Customers ( 
 customerID INT NOT NULL, 
 firstName VARCHAR(30) NOT NULL, 
 lastName VARCHAR(30) NOT NULL, 
 streetAddress VARCHAR(100) NOT NULL,
 city VARCHAR(20) NOT NULL,
 region VARCHAR(20), 
 postalcode VARCHAR(10), 
 PhoneNo VARCHAR(25) NOT NULL, 
 Email VARCHAR(50) NOT NULL, 
 CONSTRAINT pk_Customers PRIMARY KEY(customerID),
 --add constraint to check email format
 CONSTRAINT CHK_Email CHECK(Email LIKE '%_@_%_.__%')
 );
 -- add nonclutered indexes for faster accessing of data as this will be a table with large amount of rows.
 CREATE NONCLUSTERED INDEX idx_nc_lastName ON Customers(lastName);
 CREATE NONCLUSTERED INDEX idx_nc_postalCode ON Customers(postalCode);

CREATE TABLE ArchiveCustomers ( 
 archiveCustID   INT NOT NULL IDENTITY PRIMARY KEY,
 archiveDate DATETIME,
 originalcustID INT NOT NULL, 
 firstName VARCHAR(30) NOT NULL, 
 lastName VARCHAR(30) NOT NULL, 
 streetAddress VARCHAR(100) NOT NULL,
 city VARCHAR(20) NOT NULL,
 region VARCHAR(20), 
 postalcode VARCHAR(10), 
 PhoneNo VARCHAR(25) NOT NULL, 
 Email VARCHAR(50) NOT NULL, 
  );
 -- add nonclutered indexes for faster accessing of data as this will be a table with large amount of rows.
 CREATE NONCLUSTERED INDEX idx_nc_lastName ON ArchiveCustomers(lastName);
 CREATE NONCLUSTERED INDEX idx_nc_postalCode ON ArchiveCustomers(postalCode);

-- create table PolicyTypes which is a small look-up table with no Foreign Keys. 
CREATE TABLE PolicyTypes ( 
 policyTypeCode VARCHAR(6) NOT NULL, 
 policyTypeName VARCHAR(30) NOT NULL,   -- values (Comprehensive, theft, fire, third-party)
 policyTypeDescription VARCHAR(200) DEFAULT NULL, 
 
CONSTRAINT PK_PolicyTypes PRIMARY KEY(policyTypeCode)
);

 -- create table Policies;
CREATE TABLE Policies (
policyID INT NOT NULL, --(pk), 
customerID  INT NOT NULL, --(Fk), 
policyTypeCode  VARCHAR(6) DEFAULT '3PTY', --(fk), 
paymentType INT NOT NULL,  --0 monthly, 1 annually
monthlyPremium NUMERIC(6, 2) NOT NULL DEFAULT 0, -- annual premium by one payment each year is 11.5*monthly
manufactureYear INT NOT NULL, 
carModelCode VARCHAR(6) NOT NULL, -- THERE SHOULD BE A TALBE OF CARmODELcODE
poicyDescription varchar(300) DEFAULT null,
CONSTRAINT PK_Policies PRIMARY KEY(policyID)
);
 ALTER TABLE Policies ADD CONSTRAINT FK_CustomerID_Policies FOREIGN KEY (customerID) 
	REFERENCES Customers(customerID) 
		ON UPDATE CASCADE ON DELETE NO ACTION;   -- create a procedure to archive the policy/policies ON DELETE a row in Customers

ALTER TABLE Policies ADD CONSTRAINT FK_PolicyType_Policies FOREIGN KEY (policyTypeCode) 
	REFERENCES PolicyTypes(policyTypeCode) 
		ON UPDATE CASCADE ON DELETE SET DEFAULT;   

-- create ArchivePolicies table. only PK, no FK.
CREATE TABLE ArchivePolicies (
archivePID INT NOT NULL IDENTITY PRIMARY KEY,
archiveDate DATETIME,
policyID INT NOT NULL, 
customerID  INT NOT NULL, 
policyTypeCode  VARCHAR(6) NOT NULL, 
paymentType INT NOT NULL,  --0 monthly, 1 annually
monthlyPremium NUMERIC(8, 2) NOT NULL DEFAULT 0, 
manufactureYear INT NOT NULL, 
carModelCode VARCHAR(6) NOT NULL, -- THERE SHOULD BE A look-up TALBE OF CAR MODEL CODE but doesn't include in this database.
poicyDescription varchar(300),
);

--create table IncLevel for premium increase levels--another small look-up table with no Foreign Key. ;
CREATE TABLE IncLevel(
 incLevelcode INT NOT NULL,         --(PK), 
 incPercentage INT NOT NULL,          --values (0, 10, 15, 25, 50, 100);
 CONSTRAINT PK_incLevelcode PRIMARY KEY(incLevelcode)
 );
-- drop table Claims;
--create table Claims;
CREATE TABLE Claims(
claimID INT NOT NULL, 
policyID INT NOT NULL,   --fk 
incLevelcode INT DEFAULT 0,    --FK
claimDate DATETIME NOT NULL,
empID INT NOT NULL,                -- (insurance employee who processes this claim),
ReimburseDate DATETIME DEFAULT NULL, 
reimbursementAmount NUMERIC(8, 2) DEFAULT 0,
claimDetails VARCHAR(300),
CONSTRAINT PK_Claims PRIMARY KEY(claimID)
);

ALTER TABLE Claims ADD CONSTRAINT FK_policyID_Claims FOREIGN KEY (policyID) 
	REFERENCES Policies(policyID) 
		ON UPDATE CASCADE ON DELETE NO ACTION;
ALTER TABLE Claims ADD CONSTRAINT FK_incLevelcode_Claims FOREIGN KEY (incLevelcode) 
	REFERENCES IncLevel(incLevelcode) 
		ON UPDATE CASCADE ON DELETE NO ACTION;

--populate all tables except claims
-- populate table IncLevel
 INSERT INTO IncLevel
  VALUES (1, 0);
  INSERT INTO IncLevel
  VALUES (2, 10);
  INSERT INTO IncLevel
  VALUES (3, 15);
  INSERT INTO IncLevel
  VALUES (4, 25);
  INSERT INTO IncLevel
  VALUES (5, 50);
  INSERT INTO IncLevel
  VALUES (6, 100);

--POPULATE table PolicyTypes
INSERT INTO PolicyTypes(policyTypeCode, policyTypeName)
VALUES ('COMP', 'Comprehensive');
INSERT INTO PolicyTypes(policyTypeCode, policyTypeName)
VALUES ('FIRE', 'Fire');
INSERT INTO PolicyTypes(policyTypeCode, policyTypeName)
VALUES ('THFT', 'Theft');
INSERT INTO PolicyTypes(policyTypeCode, policyTypeName)
VALUES ('3PTY', 'Third Party');

-- populate table Customers
INSERT INTO Customers 
      VALUES (001, 'Charlie', 'Brown', '123 Malibu Way',	'Boston',	'Massachusetts', '978054',	'972-333-6655',	'Charlieb@rogers.com');
INSERT INTO Customers
      VALUES (002, 'Sally', 'Brown',	'123 Malibu Way',	'Boston',	'Massachusetts',	'978054',		'972-333-6655',	'sallyb@rogers.com');
INSERT INTO Customers
      VALUES (003, 'Cheryl', 'Green',	'225 Malibu Way',	'Boston',	'Massachusetts',	'978054',		'972-444-7788',	'Cherylg@rogers.com');
INSERT INTO Customers
      VALUES (007,'Cathy', 'Carins',	'229 Keats Way',	'Waterloo',	'Ontario',	'N2L3R7', '519-222-5555',	'ccarins@rogers.ca');
INSERT INTO Customers
      VALUES (004, 'Janice', 'Brown',	'225 Malibu Way',	'Boston',	'Massachusetts',	'978054', 	'972-444-7799',	'jbrown@rogers.com');
INSERT INTO Customers
      VALUES (005, 'Jen', 'Green',	'298 Malibu Way',	'Boston',	'Massachusetts',	'978054', 	'972-444-9908',	'jgreen@rogers.com');
INSERT INTO Customers
      VALUES (006, 'Josh', 'Carins',	'338 Malibu Way',	'Boston',	'Massachusetts',	'978054', '972-666-7765',	'jcarins@rogers.com');

-- populate table Policies
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18001, 005, 'COMP', 0, 86.99, 2012, 'BUIK');
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18002, 001, 'THFT', 0, 96.99, 2017, 'BUIK');
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18003, 002, '3PTY', 0, 98.99, 2006, 'BUIK');
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18004, 003, 'COMP', 0, 111.99, 2018, 'BUIK');
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18005, 004, '3PTY', 0, 102.99, 2017, 'BUIK');
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18006, 007, 'THFT', 0, 111.99, 2013, 'BUIK');
INSERT INTO Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode)
VALUES (18007, 004, 'THFT', 0, 120.99, 2013, 'CAMR');

-- leave the table Claims empty in the beginning
SELECT * FROM PolicyTypes;
SELECT * FROM IncLevel;
select * from Customers; 
SELECT * FROM Policies;
SELECT * FROM Claims;

--*************** CREATE 2 PROCEDURES AND 1 TRIGGER
--  1. A PROCEDURE
-- Create a procedure to check if a policy has already been inserted into table Policies and look up the insured cutomer's name from another table. 
-- If none, insert the policy.

--DROP PROCEDURE sp_InsertNewPolicy; 
-- create a procedure to check if a policy already exists when inserting a new one.
CREATE PROCEDURE sp_InsertNewPolicy       
			@NewPolicyID		INT,     
			@NewCustomerID		INT,
			@NewPolicyCode		VARChar(6),
			@NewPaymentType 	INT,
			@NewMPremium		NUMERIC(6,2),
			@NEWYear            INT,	
			@NEWMCode           VARCHAR(6)
	AS
	DECLARE	@RowCount		AS	Int       --declare internal variables 
	DECLARE	@PolicyID		AS	VARCHAR(10)
	DECLARE @CustFN         AS	VARCHAR(30)
	DECLARE @CustLN         AS	VARCHAR(30)
	
	-- Check to see if the policy already exists in the database
	SELECT	@RowCount = COUNT(*)
	FROM	Policies
	WHERE	policyID = @NewPolicyID    
		AND policyTypeCode = @NewPolicyCode
		AND manufactureYear = @NEWYear
		AND carModelCode =	@NEWMCode;
	
	IF (@RowCount > 0)     -- IF @RowCount > 0 THEN Customer already exists.
		BEGIN
		    SELECT @PolicyID = LTRIM(STR(policyID,10)),  @NewCustomerID = customerID,  @NewPolicyCode = policyTypeCode FROM Policies;
			SELECT @CustFN = firstName, @CustLN = lastName from Customers WHERE customerID = @NewCustomerID;
			PRINT '******************************************************'
			PRINT ''
			PRINT '   The policy with ID '  + @PolicyID + ' is already in the database. '
			PRINT ''
			PRINT '   Customer ID is ' +  LTRIM(STR(@NewCustomerID,10))+ ' with name '+ @CustFN+' '+@CustLN
			PRINT '   Policy Type is ' + @NewPolicyCode
			PRINT ' '  
			PRINT '******************************************************'
			RETURN
		END
		
	-- IF @RowCount = 0 THEN Customer does not exists in database.	
	
	ELSE
		BEGIN   -- Insert new Customer data.
			INSERT INTO dbo.Policies (policyID, customerID, policyTypeCode, paymentType, monthlyPremium, manufactureYear, carModelCode )
				VALUES(@NewPolicyID, @NewCustomerID, @NewPolicyCode, @NewPaymentType, @NewMPremium, @NEWYear, @NEWMCode);
				
			-- To get new policyID surrogate key value.
			
			SELECT	@PolicyID = LTRIM(STR(policyID,10))
			FROM	dbo.Policies
			WHERE	CustomerID = @NewCustomerID
					AND policyTypeCode = @NewPolicyCode	
					AND manufactureYear =@NEWYear
					AND carModelCode =	@NEWMCode;

			PRINT '******************************************************'
			PRINT ''
			PRINT '   The new policy ' + @PolicyID + ' is now in the database. '
			PRINT ''
			PRINT '   Customer ID is ' +  LTRIM(STR(@NewCustomerID,10))
			PRINT '   Policy Type is ' + @NewPolicyCode
			PRINT ''
			PRINT '******************************************************'
		END
		--select * from Policies where policyID = 18007
EXEC sp_InsertNewPolicy
	@NewPolicyID = 18007,     
	@NewCustomerID = 006,
	@NewPolicyCode = 'THFT',
	@NewPaymentType = 0,
	@NewMPremium = 120.99,
	@NEWYear = 2013,	
	@NEWMCode = 'CAMR'
--select * from Policies WHERE policyID =18006	
-- 	DELETE FROM Policies WHERE policyID =18006	
EXEC sp_InsertNewPolicy
	@NewPolicyID =18006,     
	@NewCustomerID = 007,
	@NewPolicyCode = 'THFT',
	@NewPaymentType = 0,
	@NewMPremium = 111.99,
	@NEWYear = 2013,	
	@NEWMCode = 'BUIK'
            
-- 2.  create trigger after Inserting a new claim
-- create a procedure at insert new claim(incRate in table Policies will change when insert, 
-- using data input and data from table policies to calculate insurance increase rate and alter the row in Policies
--DROP TRIGGER trClmsAftInsert;
CREATE TRIGGER trClmsAftInsert ON Claims
AFTER INSERT
AS
	--DECLARE @claimId INT;
	DECLARE @policyID INT;    
	DECLARE @incLevelcode INT;    
    DECLARE @claimDate DATETIME;  
	DECLARE @NewMPremium NUMERIC(8, 2);
	DECLARE @OrgMPremium NUMERIC(8, 2); -- saving original monthly premium for calcualtion   
	DECLARE @incPercentage int;
  
		SELECT @policyID = i.policyID FROM INSERTED i;	
		SELECT @incLevelcode = i.incLevelcode FROM INSERTED i;	
		SELECT @claimDate = i.claimDate FROM INSERTED i;
		SELECT @OrgMPremium = monthlyPremium FROM Policies WHERE policyID=@policyID;
		SELECT @incPercentage = incPercentage FROM IncLevel WHERE incLevelcode = @incLevelcode;
		SELECT @NewMPremium = @OrgMPremium * (1 + CAST(@incPercentage / 100.0 AS DECIMAL(8, 2))) FROM Policies WHERE policyID=@policyID;
		UPDATE Policies
			SET monthlyPremium = @NewMPremium 
			WHERE policyID=@policyID;
	PRINT LTRIM(STR(@incPercentage, 10))
	PRINT LTRIM(STR(@NewMPremium, 10))
	PRINT '*******************************************************'
	PRINT 'Policy with ID '+ LTRIM(STR(@policyID,10)) +' got updated AFTER INSERT Claim Trigger!'
	PRINT 'Premium increase rate is ' + LTRIM(STR(@incPercentage,10))+'%'
	PRINT '$'+LTRIM(STR(@OrgMPremium, 10)) +' was updated to '+'$'+LTRIM(STR(@NewMPremium, 10))+' in the table Policies.';
	PRINT '*******************************************************'
	GO
--	select* from policies;

--delete from Claims WHERE policyID = 18001;
-- Please have a look the above variable @NewMPremium. DON'T understand why my calculation doesn't work! 
INSERT INTO Claims(claimID, policyID, incLevelcode, claimDate, empID)
	VALUES (0001, 18001, 3, getdate(), 21);

-- PROCEDURE 3.
-- create a procedure to get all claims in any date range AND write to text file
CREATE PROCEDURE sp_GetClaims
(
@DateRangeBegin AS datetime,     
@DateRangeEnd AS datetime 
)
AS
SELECT * FROM Claims
WHERE claimDate >= @DateRangeBegin
	AND claimDate <= @DateRangeEnd
	   --------- claimDate is field name, @  is the value in this field
GO

EXEC sp_GetClaims '2017−04−01 00:00:00.000', '2018−12−20 00:00:00.000';

