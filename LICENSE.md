As-is Release, 2019 Lisa Wang

The released query is simply placed on public domain with no restrictions to use,  modify, merge, publish, and/or distribute as-is or modified. 

THE QUERY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND AND THE AUTHOR DOES NOT ASSUME ANY LIABILITIES WITH REGARDING ANY DIRECT OR IN-DIRECT USE OF IT. USE WITH YOUR OWN RISK.